import "./App.css";
import ShoeShop from "./ShoeShopHook/ShoeShop";

function App() {
  return <ShoeShop />;
}

export default App;
