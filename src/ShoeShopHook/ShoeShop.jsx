import React, { useState } from "react";
import Cart from "./Cart";
import { data } from "./data";
import ListShoe from "./ListShoe";

export default function ShoeShop() {
  const [cart, setCart] = useState([]);
  let handleAddToCart = (shoe) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index === -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong += 1;
    }
    setCart(cloneCart);
  };
  let handleDelete = (id) => {
    let index = cart.findIndex((item) => {
      return item.id === id;
    });
    if (index !== -1) {
      let newCart = [...cart];
      newCart.splice(index, 1);
      setCart(newCart);
    }
  };
  let handleChangeQuantity = (id, soLuong) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === id;
    });
    cloneCart[index].soLuong += soLuong;
    setCart(cloneCart);
  };
  return (
    <div className="container">
      <h2 style={{ textAlign: "center" }}>ShoeShop</h2>
      <div className="row">
        <div className="col-6">
          <Cart
            cart={cart}
            handleDelete={handleDelete}
            handleChangeQuantity={handleChangeQuantity}
          />
        </div>
        <div className="col-6">
          <ListShoe data={data} handleAddToCart={handleAddToCart} />
        </div>
      </div>
    </div>
  );
}
